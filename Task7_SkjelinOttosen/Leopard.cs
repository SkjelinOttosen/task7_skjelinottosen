﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task7_SkjelinOttosen
{
    public class Leopard : CatFamily
    {
        public Leopard(string name, string sex, string description) : base (name, sex, description)
        {

        }
        public override void Eat()
        {
            Console.WriteLine("Eating gazelle.");
        }

        public override void Sleep()
        {
            Console.WriteLine("I sleep on up to 18 hours a day.");
        }

        public override string ToString()
        {
            return $"Name: {base.Name}\nSex: {base.Sex}\nDescription: {base.Description}";
        }
    }
}
