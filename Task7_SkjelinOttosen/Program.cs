﻿using System;
using System.Collections.Generic;

namespace Task7_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creates animals
            Leopard jimmy = new Leopard("Jimmy","Male", "Jimmy is just Leopard named Jimmy");
            Cat helloKitty = new Cat("Hello Kitty","Female", "Hello Kitty is afictional cat character produced by the Japanese company Sanrio.");
            Cat tomCat = new Cat("Tom Cat","Male", "Tom Cat is a fictional character in Metro-Goldwyn-Mayer's series of Tom and Jerry theatrical animated short films.");
            Horse jollyJumper = new Horse("Jolly jumper","Male", "Jolly Jumer is horse character in the Franco - Belgian comics series Lucky Luke");

            // Creates the zoo
            Zoo zoo = new Zoo("The Greater Wynnewood Exotic Animal Park");
            zoo.AddAnimal(jimmy);
            zoo.AddAnimal(helloKitty);
            zoo.AddAnimal(tomCat);
            zoo.AddAnimal(jollyJumper);

            while(true)
            {
                Console.WriteLine("Welcome to "+zoo.Name);
                Console.WriteLine("Press p to see all the animals");
                string input = Console.ReadLine().ToLower();

                if(input=="p")
                {
                    zoo.PrintAnimalList();
                }      
            }
        }
    }
}
