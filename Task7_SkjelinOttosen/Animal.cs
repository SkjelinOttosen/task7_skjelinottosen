﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Task7_SkjelinOttosen
{
    public abstract class Animal
    {
        public string Name { get; set; }
        public string Sex { get; set; }
    
        public string Description { get; set; }

        protected Animal(string name, string sex, string description)
        {
            Name = name;
            Sex = sex;
            Description = description;
        }

        public abstract void Eat();
        public abstract void Sleep();
    }
}
