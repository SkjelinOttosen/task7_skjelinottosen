﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task7_SkjelinOttosen
{
    public class Horse : Animal
    {
        public int NumClover { get; set; }
        public Horse(string name, string sex, string description) : base(name, sex, description)
        {
            NumClover = 4;
        }
        public override void Eat()
        {
            Console.WriteLine("Eats grass and hay.");
        }

        public override void Sleep()
        {
            Console.WriteLine("Sleeping standing up.");
        }

        public override string ToString()
        {
           return $"Name: {base.Name}\nSex: {base.Sex}\nDescription: {base.Description}";
        }
    }
}
