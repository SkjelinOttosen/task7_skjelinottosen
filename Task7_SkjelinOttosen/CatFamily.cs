﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task7_SkjelinOttosen 
{
    public abstract class CatFamily : Animal
    {
        public int NumPaws { get; set; }
        public CatFamily(string name, string sex, string description) : base(name, sex, description)
        {
            NumPaws = 4;
        }
    }
}
